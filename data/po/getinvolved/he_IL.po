# 
# Translators:
msgid ""
msgstr ""
"Project-Id-Version: Ubuntu MATE Welcome\n"
"POT-Creation-Date: 2016-02-27 12:48+0100\n"
"PO-Revision-Date: 2016-03-19 13:27+0000\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: Hebrew (Israel) (http://www.transifex.com/ubuntu-mate/ubuntu-mate-welcome/language/he_IL/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: he_IL\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"Report-Msgid-Bugs-To : you@example.com\n"

#: getinvolved.html:18
msgid "Get Involved"
msgstr ""

#: getinvolved.html:26
msgid "Development"
msgstr ""

#: getinvolved.html:27
msgid ""
"This is where you can get involved with Ubuntu MATE and MATE Desktop "
"development."
msgstr ""

#: getinvolved.html:31
msgid ""
"It appears you are not connected to the Internet. Please check your "
"connection to access this content."
msgstr "נראה שאתה לא מחובר לאינטרנט. בדוק את החיבור שלך כדי לגשת לתוכן זה."

#: getinvolved.html:32
msgid "Sorry, Welcome was unable to establish a connection."
msgstr "סליחה, 'ברוך הבא' לא הצליח ליצור חיבור."

#: getinvolved.html:33
msgid "Retry"
msgstr "נסה שנית"

#: getinvolved.html:40
msgid "Ubuntu MATE Launchpad"
msgstr ""

#: getinvolved.html:48
msgid "Ubuntu MATE Bitbucket"
msgstr ""

#: getinvolved.html:56
msgid "MATE Desktop GitHub"
msgstr ""

#: getinvolved.html:63
msgid "Translations"
msgstr ""

#: getinvolved.html:66
msgid "MATE Desktop"
msgstr ""

#: getinvolved.html:71
msgid "Ubuntu MATE Applications"
msgstr ""
