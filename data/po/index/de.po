# 
# Translators:
# Elias Mardaus, 2016
# FortressBuilder <fellner.sebastian@gmail.com>, 2016-2017
# FortressBuilder <fellner.sebastian@gmail.com>, 2016
# Stephen, 2016
msgid ""
msgstr ""
"Project-Id-Version: Ubuntu MATE Welcome\n"
"POT-Creation-Date: 2016-02-27 12:48+0100\n"
"PO-Revision-Date: 2017-02-18 14:47+0000\n"
"Last-Translator: FortressBuilder <fellner.sebastian@gmail.com>\n"
"Language-Team: German (http://www.transifex.com/ubuntu-mate/ubuntu-mate-welcome/language/de/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: de\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"Report-Msgid-Bugs-To : you@example.com\n"

#: index.html:15
msgid "Main Menu"
msgstr "Hauptmenü"

#: index.html:18
msgid ""
"Ensures Welcome's documentation, translations and software picks are always "
"up-to-date."
msgstr "Stellt sicher, dass Welcomes Dokumentation, Übersetzungen und die Software-Auswahl immer auf dem neuesten Stand sind."

#: index.html:19
msgid "Subscribe to Welcome updates"
msgstr "Willkommen-Aktualisierungen abonnieren"

#: index.html:23
msgid "Welcome will automatically restart in a few moments..."
msgstr "Willkommen wird in wenigen Augenblicken automatisch neu gestartet..."

#: index.html:40
msgid "Choose an option to discover your new operating system."
msgstr "Wählen Sie eine Option aus, um Ihr neues Betriebssystem zu entdecken."

#: index.html:48
msgid "Introduction"
msgstr "Einführung"

#: index.html:49
msgid "Features"
msgstr "Eigenschaften"

#: index.html:50
msgid "Getting Started"
msgstr "Erste Schritte"

#: index.html:51
msgid "Installation Help"
msgstr "Installationshilfe"

#: index.html:56
msgid "Get Involved"
msgstr "Mitmachen"

#: index.html:57
msgid "Shop"
msgstr "Einkaufen"

#: index.html:58
msgid "Donate"
msgstr "Spenden"

#: index.html:66
msgid "Community"
msgstr "Gemeinde"

#: index.html:67
msgid "Chat Room"
msgstr "Chatraum"

#: index.html:68
msgid "Software"
msgstr "Software"

#: index.html:69
msgid "Install Now"
msgstr "Jetzt installieren"

#: index.html:76
msgid "Raspberry Pi Information"
msgstr "Raspberry-Pi-Informationen"

#: index.html:89
msgid "Open Welcome when I log on."
msgstr "Willkommen nach der Anmeldung öffnen."

#: index.html:25
msgid ""
"This Welcome application and the Boutique are being updated and will re-open"
" shortly."
msgstr "Willkommen und die Software Boutique werden aktualisiert und in Kürze wieder gestartet."
